#/users/:idへのリダイレクトを、相対パスで指定する(1)
redirect_to("/users/#{@user.id}")

#/users/:idへのリダイレクトを、絶対パスで指定する(2)
redirect_to("https://228e5b796b37495aa0c17e02856dccfa.vfs.cloud9.us-east-2.amazonaws.com/users/#{@user.id}")

#/users/:idへのリダイレクトを、user_url(id)ヘルパーを使って、絶対パスで指定する(3)
redirect_to(user_url(@user.id))

#リンクのパスとしてモデルオブジェクトが渡されると自動でidにリンクされるので、.idを省略する(4)
redirect_to(user_url(@user))

#_urlヘルパーは、省略できる(5)
redirect_to(@user)

#Rubyでは、()は省略できる(6)
redirect_to @user
