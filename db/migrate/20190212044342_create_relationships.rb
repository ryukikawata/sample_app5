class CreateRelationships < ActiveRecord::Migration[5.1]
  def change
    create_table :relationships do |t|
      t.integer :follower_id
      t.integer :followed_id

      t.timestamps
    end
    # 高速化のため
    add_index :relationships, :follower_id
    add_index :relationships, :followed_id
    # 一意性を保つため 例　同じユーザーが別のブラウザを使って2回以上フォローするのを防ぐ
    add_index :relationships, [:follower_id, :followed_id], unique: true
  end
end
