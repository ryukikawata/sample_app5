class AddAdminToUsers < ActiveRecord::Migration[5.1]
  # adminカラムを追加するとadmin?メソッドが使えるようになる
  def change
    add_column :users, :admin, :boolean, default: false
  end
end
