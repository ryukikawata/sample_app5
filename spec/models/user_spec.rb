require 'rails_helper'

RSpec.describe User, type: :model do
  # Userモデルにhas_secure_passwordを追加すると仮想的なpassword属性password_confirmation属性を追加する
  before do
    @user = User.new(name: "Example User", email: "user@example.com",
              password: "foobar", password_confirmation: "foobar")
  end

  it "should be valid" do
    expect(@user).to be_valid
  end

  it "nameがない場合" do
    @user.name = nil
    expect(@user).to be_invalid
  end

  it "emailがない場合" do
    @user.email = nil
    expect(@user).to be_invalid
  end

  it "nameの長さ50" do
    @user.name = "a" * 51
    expect(@user).to be_invalid
  end

  it "emailの長さ255" do
    @user.email = "a" * 244 + "@example.com"
    expect(@user).to be_invalid
  end

  it "emailのvalidateが正しく機能しているか" do
    valid_addresses = %w[user@example.com USER@foo.COM A_US-ER@foo.bar.org
                        first.last@foo.jp alice+bob@baz.cn]
    valid_addresses.each do |valid_address|
      @user.email = valid_address
      expect(@user).to be_valid
    end
  end

  it "emailのvalidateが正しく機能しているか2" do
    invalid_addresses = %w[user@example,com user_at_foo.org user.name@example.
                           foo@bar_baz.com foo@bar+baz.com]
    invalid_addresses.each do |invalid_address|
      @user.email = invalid_address
      expect(@user).to be_invalid
    end
  end

  it "一意性が正しく機能しているか" do
    duplicate_user = @user.dup
    duplicate_user.email = @user.email.upcase
    @user.save
    expect(duplicate_user).to be_invalid
  end

  it "emailは小文字で保存する" do
    mixed_case_email = "Foo@ExAMPle.CoM"
    @user.email = mixed_case_email
    @user.save
    expect(@user.email).to eq "foo@example.com"
  end

  it "パスワードが6文字以上で空でないか" do
    @user.password = @user.password_confirmation = "a" * 6
    expect(@user).to be_valid
  end

  it "パスワードが6文字以下なので有効でない" do
    @user.password = @user.password_confirmation = "a" * 5
    expect(@user).to be_invalid
  end
end
