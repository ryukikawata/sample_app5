require 'rails_helper'

RSpec.describe Micropost, type: :model do
  before do
    @user = User.new(name: "Example User", email: "user@example.com",
              password: "foobar", password_confirmation: "foobar")
    @micropost = Micropost.new(content: "Lorem ipsum", user_id: @user.id)
  end

  it "should be valid" do
    expect(@micropost).to be_valid
  end
end
