require 'rails_helper'

RSpec.describe "UsersLogins", type: :request do

  let(:user) { FactoryBot.create(:user) }
  # let(:other_user) { FactoryBot.create(:other_user) }

  it "sessions/newにアクセスできること" do
    #ログインページにアクセスします
    get login_path
    expect(response).to have_http_status(:success)
  end

  describe "<sessions#new>" do
    context "ログインに失敗した時" do
      it "フラッシュメッセージの残留をキャッチすること" do
        get login_path
        expect(response).to have_http_status(:success)

        #「email:""」と「password:""」の値を持ってlogin_pathにアクセスします
        # → バリデーションに引っかかりログインできません
        post login_path, params: { session: { email: "", password: "" }}
        expect(response).to have_http_status(:success)

        #flash[:danger]が表示されているかチェックします
        expect(flash[:danger]).to be_truthy
        # 違うページに移動
        get root_path
        expect(flash[:danger]).to be_falsey
      end
    end
  end

  describe "edit" do
    context "ログインされたユーザーの場合" do
      it "editページに行くこと" do
       # sign_in_as = utilities.rbのメソッド
       sign_in_as user
       get edit_user_path(user)
       expect(response).to be_success
       expect(response).to have_http_status "200"
      end
    end
    # ログインしていないユーザーの場合
    context "as a guest" do
      # ログイン画面にリダイレクトすること
      it "redirects to the login page" do
        get edit_user_path(user)
        expect(response).to redirect_to login_path
      end
    end

    # context "アカウントが違うユーザーの場合" do 通らない
    #   it "home画面にリダイレクトすること" do
    #     sign_in_as other_user
    #     get edit_user_path(user)
    #     expect(response).to redirect_to root_path
    #   end
    # end
  end
end
