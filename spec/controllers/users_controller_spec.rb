require 'rails_helper'

RSpec.describe UsersController, type: :controller do

  before do
      @user = FactoryBot.create(:user)
  end

  describe "GET #new" do
    it "returns http success" do
      get :new
      expect(response).to have_http_status(:success)
    end
  end
end
