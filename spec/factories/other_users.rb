FactoryBot.define do
  factory :other_user do
    name "Sterling Archer"

    email "duchess@example.gov"

    password "hijklnm"

    password_confirmation "hijklnm"
  end
end
