FactoryBot.define do
  factory :user do
    name "test"

    email "test@example.org"

    password "abcdefg"

    password_confirmation "abcdefg"
  end
end
