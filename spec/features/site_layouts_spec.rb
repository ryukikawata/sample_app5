require 'rails_helper'

RSpec.feature "SiteLayouts", type: :feature do

  before do
    # root_pathがpage変数に格納される
    visit root_path
  end

  scenario "Homeページに推移できるか" do
   click_link "Home"
   expect(page).to have_content "Welcome to the Sample App"
  end

  scenario "Helpページに推移できるか" do
    click_link "Help"
    expect(page).to have_content "Get help on the Ruby on Rails"
  end

  scenario "Aboutページに推移できるか" do
    click_link "About"
    expect(page).to have_content "to teach web development with"
  end

  scenario "Contactページに推移できるか" do
    click_link "Contact"
    expect(page).to have_content "Contact the Ruby on Rails Tutorial"
  end
end
