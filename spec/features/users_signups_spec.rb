require 'rails_helper'

RSpec.feature "UsersSignups", type: :feature do
  scenario "signupページに推移できるか" do
    visit root_path
    click_on "Sign up"

    expect(page).to have_content("Sign up")
  end

  scenario "ユーザー登録ができること" do
    user = FactoryBot.build(:user)
    visit signup_path

    expect {
    fill_in "Name", with: user.name
    fill_in "Email", with: user.email
    fill_in "Password", with: user.password
    fill_in "Confirmation", with: user.password_confirmation
    click_button "Create my account"
  }.to change{ User.count }.by(1)
  end

  scenario "エラーメッセージが正しく表示されること" do
    visit signup_path

    fill_in "Name", with: " "
    fill_in "Email", with: " "
    fill_in "Password", with: " "
    fill_in "Confirmation", with: " "
    click_button "Create my account"
    expect(page).to have_content("Name can't be blank")
    expect(page).to have_content("Email can't be blank")
  end

  scenario "ユーザー登録成功しshowページに移動しているか" do
    user = FactoryBot.build(:user)
    visit signup_path

    fill_in "Name", with: user.name
    fill_in "Email", with: user.email
    fill_in "Password", with: user.password
    fill_in "Confirmation", with: user.password_confirmation
    click_button "Create my account"
    expect(page).to have_content("test")
    expect(page).to have_content("Welcom to the Sample App!")
  end
end
