require 'rails_helper'

RSpec.feature "UserEdits", type: :feature do

  before do
      @user = FactoryBot.create(:user)
  end

  scenario "編集に失敗する" do
    # spec/supportのvalid_loginメソッド
    valid_login(@user)
    visit edit_user_path(@user)
    expect(page).to have_content 'Update your profile'
    fill_in 'Name', with: ''
    fill_in 'Email', with: 'foo@invalid'
    fill_in 'Password', with: 'foo'
    fill_in 'Confirmation', with: 'bar'
    click_on 'Save changes'
    expect(page).to have_content 'Email is invalid'
  end

  scenario "編集に成功する" do
    valid_login(@user)
    visit edit_user_path(@user)
    expect(page).to have_content 'Update your profile'
    fill_in "Name", with: 'Foo Bar'
    fill_in "Email", with: 'foo@bar.com'
    fill_in "Password", with: "@user.password"
    fill_in 'Confirmation', with: "@user.password"
    click_on 'Save changes'
    expect(page).to have_content ('Profile updated')
  end

  scenario "passwordが空白でも編集に成功する" do
    valid_login(@user)
    visit edit_user_path(@user)
    expect(page).to have_content 'Update your profile'
    fill_in "Name", with: 'Foo Bar'
    fill_in "Email", with: 'foo@bar.com'
    fill_in "Password", with: ""
    fill_in 'Confirmation', with: ""
    click_on 'Save changes'
    expect(page).to have_content ('Profile updated')
  end
end
