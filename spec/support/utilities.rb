module UtilitiesSupport
  def sign_in_as(user)
    post login_path, params: { session: { email: user.email,
                                      password: user.password } }
  end
end

# モジュールの定義の後にはRSpecの設定が続きます。
# ここでは、RSpec.configureを使って新しく作ったモジュール（UtilitiesSupport）をinculdeしています。

RSpec.configure do |config|
  config.include UtilitiesSupport
end
