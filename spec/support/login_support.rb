module LoginSupport
  # ログインする
  def valid_login(user)
    visit root_path
    click_link "Log in"
    fill_in "Email", with: user.email
    fill_in "Password", with: user.password
    click_button "Log in"
  end
end

# モジュールの定義の後にはRSpecの設定が続きます。
# ここでは、RSpec.configureを使って新しく作ったモジュール（LoginSupport）をinculdeしています。
# LoginSupportモジュールをincludeする
RSpec.configure do |config|
  config.include LoginSupport
end
