class SessionsController < ApplicationController
  def new
  end

  def create
    # params = { session: { password: "njoabn", email: "aklnsfc@gmail.com" } } newページのフォームから送られてきた例
    user = User.find_by(email: params[:session][:email].downcase)
    # 入力されたメールアドレスを持つユーザーがデータベースに存在し、かつ入力されたパスワードがそのユーザーのパスワードである場合
    if user && user.authenticate(params[:session][:password])
      # 有効でないユーザーがログインすることのないようにする
      if user.activated?
        log_in(user)
        # チェックボックスがオンのときに’1’になり、オフのときに’0’になります。
        params[:session][:remember_me] == '1' ? remember(user) :forget(user)
        redirect_back_or user
      else
        message = "Account not activated."
        message += "Check your email for the activation link."
        flash[:warning] = message
        redirect_to root_url
      end
    else
      flash.now[:danger] = 'Invalid email/password combination'
      render 'new'
    end
  end

  def destroy
    log_out if logged_in?
    redirect_to root_url
  end
end
