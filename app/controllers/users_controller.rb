class UsersController < ApplicationController
  before_action :logged_in_user, only:[:index, :edit, :update, :destroy, :following, :followers]
  before_action :correct_user, only:[:edit, :update]
  before_action :admmin_user, only: :destroy

  def index
    # @users = User.paginate(page: params[:page])
    # 有効なユーザーだけを表示するように変更
    @users = User.where(activated: true).paginate(page: params[:page])
  end

  def show
    @user = User.find(params[:id])
    # redirect_to root_url and return unless @user.activated?
    @microposts = @user.microposts.paginate(page: params[:page])
  end

  def new
    @user = User.new
    # debugger
  end

  def create
    # params = {:user=>{:name=>"Michael Hartl", :email=>"mhartl@example.com".....}}
    # @user = User.new(params[:user])こう書くと怒られる
    @user = User.new(user_params)
    if @user.save
      @user.send_activation_email
      flash[:info] = "Please check your email to activate your account."
      redirect_to root_url
    else
      render 'new'
    end
  end

  def edit
    # before_action :correct_user, only:[:edit, :update]でcorrect_userを先に使い
    # @user = User.find(params[:id])を行っているのでコメントアウト
    # @user = User.find(params[:id])
  end

  def update
    # @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      flash[:success] = "Profile updated"
      redirect_to @user
    else
      render 'edit'
    end
  end

  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "User deleted"
    redirect_to users_url
  end

  # render 'show_follow' 同じviewで共通のインスタンス変数を使うため
  def following
    @title = "Following"
    @user  = User.find(params[:id])
    @users = @user.following.paginate(page: params[:page])
    render 'show_follow'
  end

  # render 'show_follow' 同じviewで共通のインスタンス変数を使うため
  def followers
    @title = "Followers"
    @user  = User.find(params[:id])
    @users = @user.followers.paginate(page: params[:page])
    render 'show_follow'
  end

  private

    # Strong ParametersはDBに入れる値を制限することで、不正なパラメータの入力を防ぐ仕組みであること
    # 簡単に言うとDBへ入れたり更新したりするパラメータを制限してくれる仕組みです。
    # ①requireでPOSTで受け取る値のキーを設定
    # ②permitで許可するカラムを設定
    def user_params
      params.require(:user).permit(:name, :email, :password,
                      :password_confirmation)
    end

    def correct_user
      @user = User.find(params[:id])
      redirect_to(root_url) unless current_user?(@user)
    end

    # ログイン中のユーザーが管理者じゃなければ(adminカラムを追加するとadmin?メソッドが使えるようになる)
    # 管理者かどうか確認
    def admmin_user
      redirect_to(root_url) unless current_user.admin?
    end
end
