class Relationship < ApplicationRecord
  # class_name: "User" = followerクラスは存在しないのでその代わりにUserクラスのidに紐づける
  # follower_idとUserのidを紐づける
  # belongs_to :follower, class_name: "User" = RelationshipクラスがUserクラスを参照する
  belongs_to :follower, class_name: "User"
  belongs_to :followed, class_name: "User"
  validates :follower_id, presence: true
  validates :followed_id, presence: true
end
