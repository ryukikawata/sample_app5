class Micropost < ApplicationRecord
  belongs_to :user
  # default_scope = デフォルトの順序を指定するメソッド descで降順　ラムダ式文法
  default_scope -> { order(created_at: :desc) }
  # CarrierWave(gem)に画像と関連付けたモデルを伝えるためには、mount_uploaderというメソッドを使います。このメソッドは、引数に属性名のシンボルと生成されたアップローダーのクラス名を取ります。
  mount_uploader :picture, PictureUploader
  validates :user_id, presence: true
  validates :content, presence: true, length: { maximum: 140 }
  # 独自で用意したメソッドなのでvalidate
  validate :picture_size

  private

    def picture_size
      if picture.size > 5.megabytes
        errors.add(:picture, "should be less than 5MB")
      end
    end
end
