module UsersHelper

  # 引数で与えられたユーザーのGravater画像を返す
  # size: 80 はキーワード引数　デフォルト値は80
  def gravatar_for(user, size: 80)
    gravatar_id = Digest::MD5::hexdigest(user.email.downcase)
    gravatar_url = "https://secure.gravatar.com/avatar/#{gravatar_id}?s=#{size}"
    image_tag(gravatar_url, alt: user.name, class: "gravatar")
  end
end
