module SessionsHelper

  # 渡されたユーザーでログインする
  # railsで元から用意されているsessionメソッド(単純なログインを行えるようにする)
  # Railsのsessionメソッドを使うと、あるページから別のページに移動するときの状態を保持できる。
  # この情報はブラウザを閉じると消えてしまいます。
  def log_in(user)
    session[:user_id] = user.id
  end

  # ユーザーのセッションを永続的に復元できるようにする
  # signed = 暗号化
  # cookiesメソッドは、sessionのときと同様にハッシュとして扱えます。
  # 個別のcookiesは、１つのvalue (値) と、オプションのexpires (有効期限) からできています
  # cookies[:remember_token] = { value:   remember_token,
                             # expires: 20.years.from_now.utc }
  # expires: 20.years.from_now.utc = railsのparmanentメソッドで省略できる
  def remember(user)
    # user.rbの引数なしのrememberメソッド
    user.remember
    # user.idを代入して暗号化
    cookies.permanent.signed[:user_id] = user.id
    cookies.permanent[:remember_token] = user.remember_token
  end

  # 渡されたユーザーがログイン済みユーザーであればtrueを返す
  def current_user?(user)
    user == current_user
  end

  # 現在ログイン中のユーザーを返す (いる場合)　find_byで検索していない場合はnilを返しこのメソッドは終了する
  # @current_userに値がある場合はそのまままた代入
  def current_user
    # ただの代入 nilかfalse以外はtrue
    if user_id = session[:user_id]
      @current_user ||= User.find_by(id: user_id)
    elsif user_id = cookies.signed[:user_id] # signedメソッドで復号化
      user = User.find_by(id: user_id)
      if user && user.authenticated?(:remember, cookies[:remember_token])
        log_in user
        @current_user = user
      end
    end
  end

  # ユーザーがログインしていればtrue、その他ならfalseを返す !は否定なので逆の意味＝current_userはnilではない？
  def logged_in?
    !current_user.nil?
  end

  # 永続的セッションを破棄する
  def forget(user)
    # user.rbのforgetメソッド
    user.forget
    cookies.delete(:user_id)
    cookies.delete(:remember_token)
  end


  # 現在のユーザーをログアウトする
  def log_out
    forget(current_user)
    session.delete(:user_id)
    @current_user = nil
  end

  # 記憶したURLにリダイレクト
  def redirect_back_or(default)
    redirect_to(session[:forwarding_url] || default)
    session.delete(:forwarding_url)
  end

  # request.original_urlでリクエスト先が取得できます
  # session変数の:forwarding_urlキーに格納
  # アクセスしようとしたURLを覚えておく
  def store_location
    session[:forwarding_url] = request.original_url if request.get?
  end
end
